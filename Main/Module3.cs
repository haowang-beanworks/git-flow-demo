using System;

namespace Main
{
    public class Module3
    {
        public void Run()
        {
            Console.WriteLine("Module 3 Start");
            new CommonModule().Run();
            Console.WriteLine("Module 3 Finish");
        }
    }
}