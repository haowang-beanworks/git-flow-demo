﻿using System;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            new Module1().Run();
            new Module3().Run();
            
            Console.WriteLine("Refactor for Logging!");
            Console.WriteLine("Added ProcessOperationAttribute!");
            Console.WriteLine("Added Functional Tests!");
        }
    }
}